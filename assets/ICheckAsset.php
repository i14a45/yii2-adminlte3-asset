<?php


namespace i14a45\adminlte3\assets;


use yii\web\AssetBundle;

/**
 * icheck-bootstrap asset bundle
 * @link https://github.com/bantikyan/icheck-bootstrap
 */
class ICheckAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins/icheck-bootstrap';

    public $css = [
        'icheck-bootstrap.min.css',
    ];
}