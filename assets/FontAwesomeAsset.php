<?php


namespace i14a45\adminlte3\assets;


use yii\web\AssetBundle;

/**
 * Font Awesome asset bundle
 * @link https://fontawesome.com
 */
class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins/fontawesome-free';

    public $css = [
        'css/all.min.css',
    ];
}