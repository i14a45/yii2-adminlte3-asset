<?php

namespace app\assets;

use i14a45\adminlte3\assets\AdminLteAsset;
use i14a45\adminlte3\assets\FontAwesomeAsset;
use i14a45\adminlte3\assets\ICheckAsset;
use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700',
        'css/site.css',
    ];
    public $js = [
    ];
    public $depends = [
        AdminLteAsset::class,
        ICheckAsset::class,
        FontAwesomeAsset::class,
    ];
}
